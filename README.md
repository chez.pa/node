# GitLab CI template for Node.js

This project implements a GitLab CI/CD template to build, test and analyse your JavaScript/TypeScript/[Node.js](https://nodejs.org/) projects.

More precisely, it can be used by all projects based on [npm](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/) package managers.

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/node'
    ref: '3.7.0'
    file: '/templates/gitlab-ci-node.yml'
```

## Global configuration

The Node.js template uses some global configuration used throughout all jobs.

| Name                   | description                                                                                      | default value     |
|------------------------|--------------------------------------------------------------------------------------------------|-------------------|
| `NODE_IMAGE`           | The Docker image used to run Node.js <br/>:warning: **set the version required by your project** | `registry.hub.docker.com/library/node:lts-alpine` |
| `NODE_MANAGER`         | The package manager used by your project (npm or yarn)<br/>**If undefined, automatic detection** | _none_            |
| `NODE_CONFIG_REGISTRY` | Main npm [registry](https://docs.npmjs.com/cli/v8/using-npm/registry) to use                     | _none_            |
| `NODE_CONFIG_SCOPED_REGISTRIES` | Space separated list of npm [scoped registries](https://docs.npmjs.com/cli/v8/using-npm/scope#associating-a-scope-with-a-registry) (formatted as `@somescope:https://some.npm.registry/some/repo @anotherscope:https://another.npm.registry/another/repo`) | _none_ |
| `NODE_PROJECT_DIR`     | Node project root directory                                                                      | `.`               |
| `NODE_SOURCE_DIR`      | Sources directory                                                                                | `src`             |
| `NODE_INSTALL_EXTRA_OPTS`| Extra options to install project dependencies (either [`npm ci`](https://docs.npmjs.com/cli/ci.html/) or [`yarn install`](https://yarnpkg.com/cli/install)) | _none_ |

### Using scoped registries

Scoped registries allow to pull and publish packages using multiple registries.

Examples:

* `npm install foo` installs `foo` package from https://www.npmjs.com/ by default,
* `npm install @angular/core` installs `@angular/core` package from https://www.npmjs.com/ if no npm registry associated to scope `@angular` is declared,
* `npm install @acme-corp/bar` installs `@acme-corp/bar` package from https://registry.acme.corp/npm if this registry url is associated to scope `@acme-corp`.

First of all, be aware that the Node.js template automatically configures the [GitLab's project-level npm packages registry](https://docs.gitlab.com/ee/user/packages/npm_registry/) associated to a scope corresponding to the root of the project (ex: project `https://gitlab.example.com/my-org/engineering-group/team-amazing/analytics` will have GitLab's project-level npm packages registry scope `@my-org`).
Therefore, GitLab's project-level npm packages registry can freely be used both to install packages (with the right scope) or even to [publish](#node-publish-job) your own packages.

You may configure additional [scoped registries](https://docs.npmjs.com/cli/v8/using-npm/scope#associating-a-scope-with-a-registry) with the `$NODE_CONFIG_SCOPED_REGISTRIES` variable.
The value is expected as a (whitespace-separated) list of `@registry-scope:registry-url`.

The Node.js template also supports authentication tokens for each, simply by defining `NODE_REGISTRY_<SCOPE>_AUTH` (as project or group secret variables).

:warning: The `<SCOPE>` part is the `registry-scope` transformed in [SCREAMING_SNAKE_CASE](https://en.wikipedia.org/wiki/Snake_case) (uppercase words separated by underscores).

Example: 

```yml
variables:
  NODE_CONFIG_SCOPED_REGISTRIES: "@public-repo:https://public.npm.registry/some/repo @my-priv-repo:https://private.npm.registry/another/repo"
  # NODE_REGISTRY_MY_PRIV_REPO_AUTH set as a project secret variables
```

## Jobs

### `node-lint` job

The Node template features a job `node-lint` that performs Node.js source code **lint**. This job is **disabled by default**. It can be activated by setting `NODE_LINT_ENABLED`

It is bound to the `test` stage, and uses the following variable:

| Name                     | description                                                                                                                                                                                                                | default value                 |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------|
| `NODE_LINT_ENABLED`      | Set to `true` to enable lint analysis                                                                                                                                                                                      | _none_ (disabled)             |
| `NODE_LINT_ARGS`         | npm [run script](https://docs.npmjs.com/cli/v8/commands/npm-run-script) arguments to execute the lint analysis <br/> yarn [run script](https://classic.yarnpkg.com/en/docs/cli/run) arguments to execute the lint analysis | `run lint`                    |

The job generates a lint report that you will find here: `NODE_PROJECT_DIR/reports/node-lint.xslint.json`.

### `node-build` job

The Node template features a job `node-build` that performs **build and tests** all at once. You can disable the build using the variable **NODE_BUILD_DISABLED**

Those stages are performed in a single job for **optimization** purpose (it saves time) and also
for jobs dependency reasons (some jobs such as SONAR analysis have a dependency on test results).

This job is bound to the `build` stage, and uses the following variables:

| Name                          | description                                                                                                                                                       | default value         |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------|
| `NODE_BUILD_DISABLED`         | Set to `true` to disable build                                                                                                                                    | _none_ (enabled)      |
| `NODE_BUILD_DIR`              | Variable to define build directory                                                                                                                                | `dist`                |
| `NODE_BUILD_ARGS`             | npm [run script](https://docs.npmjs.com/cli/v8/commands/npm-run-script) arguments <br/> yarn [run script](https://classic.yarnpkg.com/en/docs/cli/run) arguments  | `run build --prod`    |
| `NODE_TEST_ARGS`              | npm [test](https://docs.npmjs.com/cli/v8/commands/npm-test) arguments <br/> yarn [test](https://classic.yarnpkg.com/en/docs/cli/test) arguments                   | `test -- --coverage`  |


#### Unit Tests and Code Coverage reports

This chapter details the required configuration (depending on the unit testing framework you're using) in
order to integrate your [unit tests reports](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) and [code coverage reports](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html) to GitLab.

Additionally, if also using SonarQube, you'll have to enable some extra reporters.

#### Unit testing with Jest

Here is the required configuration if you're using [Jest](https://jestjs.io/) as unit testing framework.

| Reporter         | Needs `npm install` | Expected report file  | Usage             |
| ---------------- | --------------------- | ----------------- | ----------------- |
| [jest-junit](https://github.com/jest-community/jest-junit) | Yes | `reports/node-test.xunit.xml` | [GitLab unit tests integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) _(JUnit format)_ |
| istanbul [text](https://istanbul.js.org/docs/advanced/alternative-reporters/#text) | No | N/A _(stdout)_ | [GitLab MR test coverage results](https://docs.gitlab.com/ee/ci/pipelines/settings.html#merge-request-test-coverage-results) _(GitLab grabs coverage from stdout)_ |
| istanbul [cobertura](https://istanbul.js.org/docs/advanced/alternative-reporters/#cobertura) | No | `reports/cobertura-coverage.xml` | [GitLab code coverage integration](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html) _(Cobertura format)_ |
| [jest-sonar](https://github.com/sh33dafi/jest-sonar)       | Yes | `reports/node-test.sonar.xml` | [SonarQube unit tests integration](https://docs.sonarqube.org/latest/analysis/generic-test/) _(generic SonarQube format)_ |
| istanbul [lcovonly](https://istanbul.js.org/docs/advanced/alternative-reporters/#lcovonly) | No | `reports/lcov.info` | [SonarQube code coverage integration](https://docs.sonarqube.org/latest/analysis/test-coverage/javascript-typescript-test-coverage/) _(JS/TS LCOV format)_ |

Here is an example of a `jest.config.js` configuration file with all the above reporters configured as expected:

```js
  reporters: [
    "default",
    // 'jest-junit' to enable GitLab unit test report integration
    [
      "jest-junit",
      {
          outputDirectory: "reports",
          outputName: "node-test.xunit.xml",
      },
    ],
    // [OPTIONAL] only if using SonarQube
    // 'jest-sonar' to enable SonarQube unit test report integration
    [
      "jest-sonar",
      {
          outputDirectory: "reports",
          outputName: "node-test.sonar.xml",
      },
    ],
  ],
  coverageDirectory: "reports",
  coverageReporters: [
    // 'text' to let GitLab grab coverage from stdout
    "text",
    // 'cobertura' to enable GitLab test coverage visualization
    "cobertura",
    // [OPTIONAL] only if using SonarQube
    // 'lcovonly' to enable SonarQube test coverage reporting
    "lcovonly",
  ],
```

#### Unit testing with Mocha

Here is the required configuration if you're using [Mocha](https://mochajs.org/) as unit testing framework.

| Reporter         | Needs `npm install` | Expected report file  | Usage             |
| ---------------- | --------------------- | ----------------- | ----------------- |
| [mocha-junit-reporter](https://github.com/michaelleeallen/mocha-junit-reporter) | Yes | `reports/node-test.xunit.xml` | [GitLab unit tests integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) _(JUnit format)_ |
| istanbul [text](https://istanbul.js.org/docs/advanced/alternative-reporters/#text) | Yes (in `nyc` package) | N/A _(stdout)_ | [GitLab MR test coverage results](https://docs.gitlab.com/ee/ci/pipelines/settings.html#merge-request-test-coverage-results) _(GitLab grabs coverage from stdout)_ |
| istanbul [cobertura](https://istanbul.js.org/docs/advanced/alternative-reporters/#cobertura) | Yes (in `nyc` package) | `reports/cobertura-coverage.xml` | [GitLab code coverage integration](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html) _(Cobertura format)_ |
| [mocha-sonarqube-reporter](https://github.com/mmouterde/mocha-sonarqube-reporter)       | Yes | `reports/node-test.sonar.xml` | [SonarQube unit tests integration](https://docs.sonarqube.org/latest/analysis/generic-test/) _(generic SonarQube format)_ |
| istanbul [lcovonly](https://istanbul.js.org/docs/advanced/alternative-reporters/#lcovonly) | Yes (in `nyc` package) | `reports/lcov.info` | [SonarQube code coverage integration](https://docs.sonarqube.org/latest/analysis/test-coverage/javascript-typescript-test-coverage/) _(JS/TS LCOV format)_ |

:warning: Remarks:

1. By default - unlike Jest - Mocha doesn't provide code coverage. To do so you need to install [Istanbul](https://www.npmjs.com/package/nyc) package (`nyc`):
    ```shell
    npm install --save-dev nyc
    ```
2. the default `xunit` Mocha reporter doesn't produce a JUnit format supported by GitLab, that's why we recommend you to use  `mocha-junit-reporter` instead.
3. Mocha doesn't support multiple unit tests reporters. 
So unfortunaltely, if you're using SonarQube, you'll have to choose which report you want to generate. 
Another option is to use [mocha-multi-reporters](https://github.com/stanleyhlng/mocha-multi-reporters) (see documentation)

Mocha may be either configured with CLI options of using separate Mocha and `nyc` config files.

Here is the required configuration with CLI options directly in the `package.json` file:

```json
  "scripts": {
    "test": "npm run mocha",
    "mocha": "nyc --report-dir=reports --reporter=text --reporter=lcovonly --reporter=cobertura mocha --reporter mocha-junit-reporter --reporter-option mochaFile=reports/node-test.xunit.xml test/*.js",
    ...
  },
  ...
```

Here is the equivalent using separate config files:

* `package.json`:
    ```json
      "scripts": {
        "test": "npm run mocha",
        "mocha": "nyc mocha test/*.js",
        ...
      },
      ...
    ```
* `.mocharc.json`:
    1. with `mocha-junit-reporter` (for GitLab):
        ```json
        {
          "reporter": "mocha-junit-reporter",
          "reporter-option": ["mochaFile=reports/node-test.xunit.xml"]
        }
        ```
    2. with `mocha-sonarqube-reporter` (for SonarQube):
        ```json
        {
          "reporter": "mocha-sonarqube-reporter",
          "reporter-option": ["output=reports/node-test.sonar.xml"]
        }
        ```
    3. with both (using `mocha-multi-reporters`):
        ```json
        {
          "reporter": "mocha-multi-reporters",
          "reporter-option": ["configFile=.mmr.json"]
        }
        ```
        With `.mmr.json`:
        ```json
        {
            "reporterEnabled": "spec, mocha-junit-reporter, mocha-sonarqube-reporter",
            "mochaJunitReporterReporterOptions": {
              "mochaFile": "reports/node-test.xunit.xml"
            },
            "mochaSonarqubeReporterReporterOptions": {
              "output": "reports/node-test.sonar.xml"
            }
        }
        ```
* `.nycrc.json`:
    ```json
    {
      "reporter": ["text", "cobertura", "lcovonly"],
      "report-dir": "reports"
    }
    ```

#### Unit testing with Jasmine

Support of [Jasmine](https://jasmine.github.io/) as unit testing framework is not documented yet and will come soon in a further
version of this template.

### SonarQube analysis

If you're using the SonarQube template to analyse your Node code, here are 2 sample `sonar-project.properties` files.

If using JavaScript language:

```properties
# see: https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/javascript-typescript-test-coverage/
# set your source directory(ies) here (relative to the sonar-project.properties file)
sonar.sources=.
# exclude unwanted directories and files from being analysed
sonar.exclusions=node_modules/**,dist/**,**/*.test.js

# set your tests directory(ies) here (relative to the sonar-project.properties file)
sonar.tests=.
sonar.test.inclusions=**/*.test.js

# tests report: generic format
sonar.testExecutionReportPaths=reports/node-test.sonar.xml
# lint report: ESLint JSON
sonar.eslint.reportPaths=reports/node-lint.xslint.json
# coverage report: LCOV format
sonar.javascript.lcov.reportPaths=reports/lcov.info
```

If using TypeScript language:

```properties
# see: https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/javascript-typescript-test-coverage/
# set your source directory(ies) here (relative to the sonar-project.properties file)
sonar.sources=src
# exclude unwanted directories and files from being analysed
sonar.exclusions=node_modules/**,dist/**,**/*.spec.ts

# set your tests directory(ies) here (relative to the sonar-project.properties file)
sonar.tests=src
sonar.test.inclusions=**/*.spec.ts

# tests report: generic format
sonar.testExecutionReportPaths=reports/node-test.sonar.xml
# lint report: TSLint JSON
sonar.typescript.tslint.reportPaths=reports/node-lint.xslint.json
# coverage report: LCOV format
sonar.typescript.lcov.reportPaths=reports/lcov.info
```

More info:

* [JavaScript language support](https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/javascript-typescript-test-coverage/)
* [TypeScript language support](https://docs.sonarqube.org/latest/analyzing-source-code/test-coverage/javascript-typescript-test-coverage/)
* [test coverage & execution parameters](https://docs.sonarqube.org/latest/analysis/coverage/)
* [third-party issues](https://docs.sonarqube.org/latest/analysis/external-issues/)

### `node-audit` job

The Node template features a job `node-audit` that performs an audit ([npm audit](https://docs.npmjs.com/cli/v8/commands/npm-audit) or [yarn audit](https://classic.yarnpkg.com/en/docs/cli/audit)) to find vulnerabilities (security).

It is bound to the `test` stage.

| Name                   | description                                                                                                                                           | default value                    |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------|
| `NODE_AUDIT_DISABLED`  | Set to `true` to disable npm audit                                                                                                                    | _none_ (enabled)                 |
| `NODE_AUDIT_ARGS`      | npm [audit](https://docs.npmjs.com/cli/v8/commands/npm-audit) arguments <br/> yarn [audit](https://classic.yarnpkg.com/en/docs/cli/audit) arguments   | `--audit-level=low`              |

In addition to a textual report in the console, this job produces the following report, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `$NODE_PROJECT_DIR/reports/npm-audit.native.json` | [JSON](https://docs.npmjs.com/cli/v9/commands/npm-audit#json) | [DefectDojo integration](https://documentation.defectdojo.com/integrations/parsers/#npm-audit)<br/>_This report is generated only if DefectDojo template is detected, if needed, you can force it with `$DEFECTDOJO_NPMAUDIT_REPORTS`_ |


### `node-npm-outdated` job

The Node template features a job `node-outdated` that performs outdated analysis ([npm outdated](https://docs.npmjs.com/cli/v8/commands/npm-outdated) or [yarn outdated](https://classic.yarnpkg.com/lang/en/docs/cli/outdated/)) to find dependencies that might be updated.

It is bound to the `test` stage.

| Name                      | description                                                                                                                                                           | default value                      |
|---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|
| `NODE_OUTDATED_DISABLED`  | Set to `true` to disable npm outdated                                                                                                                                 | _none_ (enabled)                   |
| `NODE_OUTDATED_ARGS`      | npm [outdated](https://docs.npmjs.com/cli/v8/commands/npm-outdated) arguments <br/> yarn [outdated](https://classic.yarnpkg.com/lang/en/docs/cli/outdated/) arguments | `--long`                           |

The job generates an outdated report that you will find here: `NODE_PROJECT_DIR/reports/npm-outdated-report.json`.

### `node-sbom` job

This job generates a [SBOM](https://cyclonedx.org/) file listing installed packages using [@cyclonedx/cyclonedx-npm](https://www.npmjs.com/package/@cyclonedx/cyclonedx-npm).

It is bound to the `test` stage, and uses the following variables:

| Name                  | description                            | default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `NODE_SBOM_DISABLED` | Set to `true` to disable this job | _none_ |
| `NODE_SBOM_VERSION` | The version of @cyclonedx/cyclonedx-npm used to emit SBOM | _none_ (uses latest) |
| `NODE_SBOM_OPTS` | Options for @cyclonedx/cyclonedx-npm used for SBOM analysis | `--omit dev` |

### `node-publish` job

This job [publishes](https://docs.npmjs.com/cli/v8/commands/npm-publish) the project packages to a npm registry.

This job is bound to the `publish` stage and is disabled by default.
When enabled, it is executed on a Git tag with a semantic versioning pattern (`v?[0-9]+\.[0-9]+\.[0-9]+`, _configurable_).

It uses the following variables:

| Name                       | description                                                                 | default value     |
|----------------------------|-----------------------------------------------------------------------------|-------------------|
| `NODE_PUBLISH_ENABLED`     | Set to `true` to enable the publish job                                     | _none_ (disabled) |
| `NODE_PUBLISH_ARGS`        | npm [publish](https://docs.npmjs.com/cli/v8/commands/npm-publish) extra arguments<br/>yarn [publish](https://classic.yarnpkg.com/lang/en/docs/cli/publish/) extra arguments | _none_ |
| :lock: `NODE_PUBLISH_TOKEN`| npm publication registry authentication token                              | _none_ |

#### Configure the target registry

The target registry url for publication shall be configured in the `publishConfig` of your `package.json` file. Examples:

* for an unscoped package:
    ```json
    {
      "name": "my-package",
      // ...
      "publishConfig": {
        "registry": "https://registry.acme.corp/npm"
      }
      // ...
    }
    ```
* for a scoped package:
    ```json
    {
      "name": "@acme/my-package",
      // ...
      "publishConfig": {
        "@acme:registry": "https://registry.acme.corp/npm"
      }
      // ...
    }
    ```

Then simply declare the registry authentication token with :lock: `NODE_PUBLISH_TOKEN`.

:information_source: it is not mandatory to declare the registry if you wish to use the GitLab 
project-level npm packages registry (it is declared by default by the template, with the required credentials). All you have to do to is to make sure your npm package name 
 [uses the right scope](https://docs.gitlab.com/ee/user/packages/npm_registry/#naming-convention).
For example, if your project is `https://gitlab.example.com/my-org/engineering-group/team-amazing/analytics`, the root namespace is `my-org`, and your package name must have the `@my-org` scope (probable package fullname: `@my-org/analytics`).

#### Exclude resources from package

Don't forget to exclude undesired folders and files from the package resources (simply add them to your `.gitignore` or `.npmignore` file):

* the `.npm/` or `.yarn/` folder, that is used internally by the Node template to store `npm` or `yarn` cache (depending on the package manager you're actually using),
* the `reports/` folder, that is used by most _to be continuous_ to output all kind of reports,
* the Node.js build output dir (if any),
* any other undesired file & folder that you don't want to appear in your published package(s).
